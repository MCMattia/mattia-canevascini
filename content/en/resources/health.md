---
title: "Life, Psicology, Health and Choices"
date: 2020-10-23T14:40:51+02:00
draft: false
description: "Resources for a wide range of health topics. Psicology, Sociology, Relationships, Work and passion, Iron, Borelliosis, Toxicity, Mercury, Aluminium, Diet, EMF Electro-magnetic fields. Written by  Mattia Canevascini."
author: "Mattia Canevascini"
---

Resources for a Wide range of health topics.
<!--more-->

### Psicology, Sociology, Relationships 
*Book* The Subtle Art of Not Giving a F*uck, Mark Manson 

*Book* Think and grow rich, Napoleon Hill 

*Book* How to win friends and influence people, Dale Carnegie

### Work and passion
*Book* So good they can't ignore you, Cal Newport  

### Silent killer 1: Iron
*Website* [Rogue health and fitness](https://roguehealthandfitness.com/category/iron)

### Silent killer 2: Borelliosis
*Website* [Dr. Dietrich Klinghardt ](http://www.klinghardtacademy.com/Lyme-Disease-by-Klinghardt/)

### Silent killer 3: Toxicity
*Website* [Andy Cutler](https://andy-cutler-chelation.com/)

*Article* [Andy Cutler, Weston a Price Foundation](https://www.westonaprice.org/health-topics/environmental-toxins/using-the-andy-cutler-protocol-to-address-mercury-poisoning/)

### Diet 
*Book* The Story of the Human Body: Evolution, Health, and Disease, Daiel E. Lieberman
 
*Book* Nutrition and physical degeneration, Weston A. Price 

*Book* Nourishing traditions, Sally Fallon 

*Website* [Weston a Price foundation](https://www.westonaprice.org/)


### EMF: Smartphones, Routers and Wi-Fii
*Book* The non tinfoil guide to EMFs, Nicolas Pineault

*Website*  [electrosmogtech, Olivier Bodenmann](https://www.electrosmogtech.ch)

*PDF* [Cell tower radiation](/en/pdf/Cell_Tower_Radiation.pdf)

*PDF* [Bioinitiative 2012 summary](/en/pdf/Bioinitiative_2012_sum.pdf)

*PDF* [Bioinitiative 2012 full report](/en/pdf/Bioinitiative_2012.pdf)

&nbsp;

**Written by Mattia Canevascini**
