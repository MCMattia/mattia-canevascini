---
title: "Privacy"
date: 2020-10-27T15:23:36+01:00
draft: false
description: "Resources for privacy topics. De-google. Intel Backdoor Inside, Intel ME. Written by Mattia Canevascini"
author: "Mattia Canevascini"
---

Resources for privacy topics.
<!--more-->

### De-google
[PrivacyTools](https://www.privacytools.io/)

### Intel Backdoor Inside
[Intel ME, hackday](https://hackaday.com/2017/12/11/what-you-need-to-know-about-the-intel-management-engine/)

[Intel ME, how to geek](https://www.howtogeek.com/334013/intel-management-engine-explained-the-tiny-computer-inside-your-cpu/)

&nbsp;

**Written by Mattia Canevascini**
