---
date: 2020-10-23T11:33:37+02:00
Tags: []
draft: false
description: Ciao! I am an electronical engineer, teacher and independent researcher 
author: "Mattia Canevascini"
---


# Ciao! ✌️
### I am an electronical engineer, teacher and independent researcher 

### Study
I am about to complete a bachelor in electronic engineering at the HES-SO university in Fribourg.

### Private lessons
I teach the following courses
- Mathematics 
- Physics
- Electronic
- Electrotechnic

for students from obbligatoy schools, professional schools, apprentiship end collage.

### Independent researcher
I am currently writing a book about a holistic cure for acne, other articles and resources that I wrote are pubblicly availabe on this website.

### Contact me
For more information feel free to contact me by email: 
mattiacanevascini@tutanota.com
