---
title: "Vita, Psicologia, Salute e Scelte"
date: 2020-10-23T14:40:51+02:00
draft: false
description: "
. Psicologia Sociologia Relazioni. Lavoro e passioni. Silent killer. Borelliosi. Intossicazioni. ferro. Alimentazione. Campi Elettro magnetici. Redatto da Mattia Canevascini."
author: "Mattia Canevascini"
---


Risorse in diversi campi umanitari.
<!--more-->

### Psicologia, Sociologia, Relazioni
[Libro] The Subtle Art of Not Giving a F*uck, Mark Manson 

[Libro] Think and grow rich, Napoleon Hill 

[Libro] How to win friends and influence people, Dale Carnegie

### Lavoro e passioni
[Libro] So good they can't ignore you, Cal Newport  

### Silent killer 1: Ferro
[Sito internet] [Rogue health and fitness](https://roguehealthandfitness.com/category/iron)

### Silent killer 2: Borelliosi
[Sito] [Dr. Dietrich Klinghardt ](http://www.klinghardtacademy.com/Lyme-Disease-by-Klinghardt/)

### Silent killer 3: Intossicazioni
[Sito internet] [Andy Cutler](https://andy-cutler-chelation.com/)

[Articolo] [Andy Cutler, Weston a Price Foundation](https://www.westonaprice.org/health-topics/environmental-toxins/using-the-andy-cutler-protocol-to-address-mercury-poisoning/)

### Alimentazione
[Libro] The Story of the Human Body: Evolution, Health, and Disease, Daiel E. Lieberman
 
[Libro] Nutrition and physical degeneration, Weston A. Price 

[Libro] Nourishing traditions, Sally Fallon 

[Sito internet]  [Weston a Price foundation](https://www.westonaprice.org/)


# EMF: Telefoni, Routers, Wi-Fii ed Antenne 
[Libro] The non tinfoil guide to EMFs, Nicolas Pineault

[Sito internet]  [electrosmogtech, Olivier Bodenmann](https://www.electrosmogtech.ch)

*PDF* [Radiazioni antenne cellulari](/en/pdf/Cell_Tower_Radiation.pdf)

*PDF* [Bioinitiative 2012 riassunto](/en/pdf/Bioinitiative_2012_sum.pdf)

*PDF* [Bioinitiative 2012 rapporto completo](/en/pdf/Bioinitiative_2012.pdf)

&nbsp;

**Redatto da Mattia Canevascini**
