+++
draft = false
image = "images/study/study.jpg"
date = "2020-04-05"
title =  "Ottieni risultati migliori studiando meno"
weight = 1
description = "In questo articolo spiego come ottimizzare il proprio metodo di studio. Aumentare l'efficenza dello studio porta a migliori risultati, alla comprensione più veloce di concetti chiave e di conseguenza a più tempo libero. Interesse. Esame. Redatto da Mattia Canevascini."
author = "Mattia Canevascini"
+++

In questo articolo spiego come ottimizzare il proprio metodo di studio. Aumentare l'efficenza dello studio porta a migliori risultati, alla comprensione più veloce di concetti chiave e di conseguenza a più tempo libero.
<!--more-->

## Il Piano
Pianifica un determinato lasso di tempo che dedicherai totalmente allo studio. Potrebbero essere 25 minuti o 2 ore di compiti, lettura, programmazione, scrittura ecc.

Poniti un obbiettivo **specifico** e **raggiungibile**, per esempio facendo gli esercizi c) e d) o leggere il capitolo 11. Porsi un obbiettivo specifico farà in modo che il progresso sia tangibile e soddisfacente.

Blocca ogni distrazione. Imposta il telefono in modalità aereo in modo che nessuna notifica ti distragga. Non avere cibo o altre distrazioni vicino a te.

Così facendo, renderai il tempo di studio più produttivo e raggiungerai più velocemente gli obbiettivi posti.

## Concentrazione
La concentrazione è una grande abilità, a volte difficile da raggiungere. Eliminare le distrazioni come descritto prima aiuta molto. Generalmente però la concentrazione è un'abilità da sviluppare e migliorare con il tempo.  

Un metodo efficace per migliorare la propria concentrazione consiste nell'analizzare e modificare il proprio ambiente di studio. Studiare nella stessa camera in cui giochi, guardi serie TV o controlli i social, risulterà molto difficile ed improduttivo. Questo accade perchè la mente associa l'ambiente con le attività praticate in esso. Ti sarai già accorto di questo fenomeno se hai già cercato di leggere un libro la sera sul tuo letto; ti sarai sentito subito stanco e avrai avuto voglia di dormire. Se ti siedi su una poltrona invece riuscirai a rimanere concentrato. Il tuo corpo associa il letto ad uno stato di sonnolenza e la poltrona invece ad uno stato neutro che ti permette di rimanere concentrato. Molte persone preferiscono studiare in biblioteca proprio peché la bilblioteca viene associata con uno stato di lavoro e studio, quindi risulta più facile concentrarsi.

## Interessante = Facile 
Imparare qualche cosa che ti piace, un nuovo sport o un soggetto di studio che trovi particolarmente interessate è molto più facile rispetto a qualche cosa che non ti piace. Se non ti piace l'argomento nel quale vuoi migliorare, devi trovare un modo per renderlo il più interessante possibile. Cerca un collegamento con i tuoi interessi, fai domande al Prof. su possibili applicazioni o parla con qualcuno che conosce molto bene l'argomento.

## Padroneggialo 
Se vuoi padroneggiare l'argomento, c'è uno step addizionale che puoi adottare. In sostanza devi capire l'argomento così bene da poterlo spiegare ad altri. Spiegalo a persone che non lo conoscono e a persone che lo conoscono come o meglio di te. Così facendo scoprirai le lacune nelle tue conoscenze. Colmarle perfezionerà il tuo sapere e ti darà maggior sicurezza.

## L'esame
Il giorno dell'esame dovresti essere sicuro delle tue conoscenze, ma soprattutto **devi aver dormito a sufficienza**. Solo così sarai capace di tenere alta la concentrazione e di massimizzare i risultati.

Prima e durante l'esame rilassati e non entrare in panico. Se rilassarti non ti risulta facile, respira in modo leggero e lento utilizzando il diaframma. Ciò ti calmerà. Puoi continuare a fare questo esercizio durante l'esame.

Se ti sembra che l'esame è troppo lungo o che non è ciò che ti aspettavi, convinciti che farai del tuo meglio in ogni caso. Se puoi, priorizza gli esercizi che sai di saper fare.

## In Conclusione
Sperimenta i concetti che hai appena letto. Trova un metodo che funziona bene per te. Migliorerai i risultati e studierai meno ore. 

&nbsp;

**Redatto da Mattia Canevascini**

