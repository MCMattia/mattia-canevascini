---
title: "L'alimentazione che dà risultati"
date: 2019-07-30T16:52:23+02:00
image: ""
author: "Mattia Canevascini"
noindex: true
draft: true
---


Per seguire un'alimentazione sana è necessario innanzitutto individuare quali cibi evitare ed in seguito quali cibi prediligere. L'approccio alimentare che viene descritto in questo articolo, se adottato correttamente, porta a risultati tangibili già nelle prime settimane. Non c'è da stupirsi quindi che questa alimentazione sia diversa dalle ordinarie diete low-fat e vegetariane che portano a pochi risultati e zero benefici.
<!--more-->
 
Questo articolo è suddiviso nei seguenti capitoli: 


* **Cibi da evitare**
* **Cibi da prediligere**
* **Un'alimentazione che funziona**


## **Cibi da evitare**
In generale, ogni cibo creato in fabbrica o/e modificato geneticamente non può far parte di un'alimentazione sana. 

#### Olii vegetali
Gli olii vegetali, quali: olio di girasole, di semi di soia, di palma, di colza e la margarina non devono essere consumati in quanto sono tossici per il corpo. La consumazione di questi grassi porta ad infiammazione interna, aumento di peso e a malattie degenerative. 

#### Zuccheri
Gli zuccheri: bianco, di canna, destrosio, fruttosio, sciroppo di mais ad alto fruttosio e dolcificanti di vari tipi sono anch'essi da eliminare completamente, in quanto tossici.

#### Eccesso di carboidrati
Oggi, oltre al cibo chimico, la popolazione media consuma grandi quantità di carboidrati. Pasta, dolci, pane, cereali, ecc. 
I prodotti a base di cereali e di legumi devono essere ridotti considerevolmente o eliminati dall'alimentazione soprattutto se l'obbiettivo è quello di perdere peso o di curare problemi della pelle. 
 

## **Cibi da prediligere**
I cibi da prediligere sono tutti i cibi che provengono dal regno animale. Essi sono ricchi di vitamine e minerali e soddisfano il corpo, fornendo energia e vitalità. Carni fresche, salumi artigianali, pesci da pesca **selvatica**, uova e formaggi contengono i nutrienti che il corpo necessita per funzionare al meglio. L'alimentazione deve essere basata su questi prodotti per garantire un buon livello di salute. Oltre ai prodotti animali è possibile avere un contorno di verdura di stagione coltivata localmente. 

La carne da preferire è rossa e grassa. La carne degli animali ruminanti contiene generalmente più vitamine e minerali. Inoltre il grasso presente ha un profilo nutrizionale migliore rispetto a quello delle carni bianche. Se possibile preferire animali allevati in condizioni naturali, quindi alimentati da erba e fieno. I salumi non devono contenere un numero eccessivo di conservanti ed altre sostanze chimiche. I formaggi sono da preferire a base di latte crudo (Parmigiano Reggiano, Gruyère, Sbrinz, Grana Padano, etc.) siccome sono più facili da digerire, contengono più nutrienti e sono generalmente più buoni. I grassi contenuti nella carne e nei formaggi sono da consumare senza timore, in quanto sono la fonte di energia primaria quando i carboidrati sono ridotti.

Nel caso in cui il latte ed i prodotti derivati non sono ben tollerati è possibile provare formaggi a base di latte crudo e latte non pastorizzato. Essi pongono generalmente meno problemi siccome contengono enzimi e batteri che aiutano la digestione degli stessi. In alternativa è possibile provare il latte di capra o pecora non pastorizzato. Solitamente risulta più facile da digerire rispetto al latte di mucca.

Se il latte ed i suoi derivati non sono ben tollerati è necessario evitarne il consumo. È imperativo non consumare forme artificiali di latte, quali: il latte di soia, di riso, di mandorle ecc. Il calcio contenuto nel latte non è essenziale per il corpo e non deve essere introdotto in maniera artificiale. Basti pensare agli eschimesi.


## **Un'alimentazione che funziona**
Per essere in buona salute e sentirsi bene, l'alimentazione non è l'unico fattore da considerare, ma è certamente uno dei più importanti. Adottando uno stile di alimentazione come quello descritto, i benefici sono tangibili già dopo qualche settimana. 
Quest'alimentazione funziona così bene, innanzitutto perché rimuove i cibi tossici, quali: oli vegetali, zuccheri ed in generale i cibi creati in fabbrica e secondariamente siccome alimenta l'individuo con nutrienti di alta qualità contenuti nei prodotti animali. 

![alt text](https://33q47o1cmnk34cvwth15pbvt120l-wpengine.netdna-ssl.com/wp-content/uploads/liver.jpeg "paragone tra piante e carni")

Come si evince dalla tabella, la carne contiene un numero maggiore di nutrienti rispetto ai mirtilli ed al cavolo riccio, entrambi considerati molto sani.
Inoltre, i nutrienti presenti nei prodotti animali sono generalmente assorbiti in modo più efficiente dal corpo.

Questo approccio alimentare, limitando i carboidrati obbliga il metabolismo ad utilizzare una fonte di energia diversa dallo zucchero. Nelle diete di oggi, ricche di carboidrati, il corpo converte i carboidrati ingeriti in zucchero e quest'ultimo alimenta il corpo. Invece, limitando i carboidrati, il metabolismo non può più convertire i carboidrati in zuccheri e quindi cambia *combustibile*. Il corpo entra in uno stato chiamato di chetosi dove l'energia viene convertita dai grassi. Ciò comporta livelli più stabili di energia durante il giorno, fame ridotta ed una facilità nel perdere il peso in eccesso.

Questo approccio alimentare simula l'alimentazione dell'uomo antico prima dell'avvento dell'agricoltura. È ben noto che prima dell'agricoltura i problemi di salute che conosciamo oggi (acne, obesità, diabete, problemi cardiovascolari, cancro, etc.), non esistevano. Inoltre, questa è l'alimentazione delle tribù ancora esistenti che non sono in contatto con la civiltà. Eschimesi e Maasai sono due ottimi esempi, hanno accesso principalmente ai cibi descritti nel capitolo *cibi da prediligere* e vantano di un'ottima salute. 

![alt text](https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F4%2F4f%2FEskimo_Family_NGM-v31-p564.jpg%2F1200px-Eskimo_Family_NGM-v31-p564.jpg&f=1 "Logo Title Text 1")

Le informazioni contenute in questo articolo permettono ad ognuno di capire che cosa sia un'alimentazione sana ed iniziare a sperimentare personalmente. 

Se hai domande puoi scrivere un commento sotto l'articolo o contattarmi personalmente attraverso la pagina *contatto*.  

&nbsp;

**Written by Mattia Canevascini**

